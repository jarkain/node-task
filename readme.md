# Node task

The purpose of this task was to test basic Markdown features.

## Bacon ipsum dolor amet drumstick 

1. cow porchetta filet mignon pork `git clone`
2. loin **meatball** ham *meatloaf* shoulder
3. jerky ham hock picanha

- Shankle doner spare ribs short ribs ham.
- Chislic ham hock ribeye pork chop

* With * also

```javascript
// javascript code formatting
const fuu = "bar"
```

![text of hello as image](hello.png)

> Cow ground round meatloaf, shank frankfurter

## Hamburger sausage leberkas

- [x] Swine landjaeger shank pig flank tongue
- [ ] Jerky ribeye spare ribs